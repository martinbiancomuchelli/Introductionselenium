import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import java.time.Duration;

public class Locators2 {
    public static void main(String[] args) throws InterruptedException {
        // TODO Auto-generated method stub
        System.setProperty("webdriver.chrome.driver", "C:\\Drivers\\chromedriver_win32\\chromedriver.exe");
        System.setProperty("webdriver.http.factory", "jdk-http-client");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));

        driver.get("https://rahulshettyacademy.com/locatorspractice/");

        driver.findElement(By.id("inputUsername")).sendKeys("martinbiancomuchelli@gmail.com");

        driver.findElement(By.name("inputPassword")).sendKeys("rahulshettyacademy"); //"CursoTesting_2022");

        driver.findElement(By.className("signInBtn")).click();

        Thread.sleep(2000);

        //Forma en que localizamos elementos en URG.
        //System.out.println( driver.findElement(By.xpath("//p[normalize-space()='You are successfully logged in.']")));
        //driver.findElement(By.xpath( "//p[normalize-space()='You are successfully logged in.']")));

       System.out.println(driver.findElement(By.tagName("p")).getText());
        Assert.assertEquals(driver.findElement(By.tagName("p")).getText(),"You are successfully logged in.");

        Thread.sleep(2000);
        driver.findElement(By.xpath("//button[normalize-space()='Log Out']")).click();
        System.out.println(By.xpath("button[normalize-space()='Log Out'])).click()"));
        driver.close();



    }
}
